//Enum pour les jours de la semaine et ses fonctions
enum Day { MON,TUE, WED, THU, FRI, SAT, SUN, NONE};
char* day_to_string(enum Day d);
enum Day string_to_day(char* d);

//Maximal size of the string as they appear in the bdd
#define MAX_STRLEN_DAY 8
#define MAX_STRLEN_NAME 20
#define MAX_STRLEN_ACTIVITY 20
#define MAX_STRLEN_HOUR 2
#define MAX_STRLEN_COMMA 3
#define MAX_STRLEN_ENTRY MAX_STRLEN_DAY + MAX_STRLEN_HOUR + MAX_STRLEN_NAME + MAX_STRLEN_ACTIVITY+ MAX_STRLEN_COMMA

#define MAX_STRLEN_CMD MAX_STRLEN_ENTRY + 3
//Structure pour les différents champs d'une donnée
struct Data {
  char* name;
  char* activity;
  enum Day day;
  int hour;
};
typedef struct Data Data;

void data_free(Data* d);
void data_format(char* l, Data* data);
