#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <semaphore.h>
#include "bdd.h"
#include "utils.h"

//Nom du fichier contenant les données
static const char* DATA = "data";
sem_t * sem;
//Retourne une string à partir d'un Day 
char* day_to_string(enum Day d) {
  switch (d) {
    case MON: return "Lundi";
    case TUE: return "Mardi";
    case WED: return "Mercredi";
    case THU: return "Jeudi";
    case FRI: return "Vendredi";
    case SAT: return "Samedi";
    case SUN: return "Dimanche";
    case NONE: return "Not a day";
  }
}

//Retourne un Day à partir d'un string
//dans le cas où la string ne correspond pas à un jour, on renvoie NONE
enum Day string_to_day(char* dd) {
  char d[LINE_SIZE]; 
  strcpy(d, dd);
  //Conversion en minuscule
  for (int i = 0; i < strlen(d); i++) 
    d[i] = tolower(d[i]);
  
  if (strcmp("lundi", d) == 0) return MON;
  else if (strcmp("mardi", d) == 0) return TUE;
  else if (strcmp("mercredi", d) == 0) return WED;
  else if (strcmp("jeudi", d) == 0) return THU;
  else if (strcmp("vendredi", d) == 0) return FRI;
  else if (strcmp("samedi", d) == 0) return SAT;
  else if (strcmp("dimanche", d) == 0) return SUN;
  else return NONE;
}


// Libère la mémoire d'un pointeur vers Data
void data_free(Data* d) {
  free(d->name);
  free(d->activity);
  free(d);
}

//Modifie une chaîne de caratère correspondant à data
void data_format(char* l, Data* data) {
  sprintf(l, "%s,%s,%s,%d\n", 
      data->name, data->activity, 
      day_to_string(data->day), data->hour);
}


//Retourne une structure Data à partir d'une ligne de donnée
// get_data("toto,arc,lundi,4") ->  Data { "toto", "arc", MON, 4 };
// Attention il faudra libérer la mémoire vous-même après avoir utilisé
// le pointeur généré par cette fonction
Data* get_data(char* line) {
  char* parse;
  Data* data = malloc(sizeof(Data));
  char error_msg[LINE_SIZE];
  sprintf(error_msg, "Erreur de parsing pour: %s\n", line);
  
  //On s'assure que la ligne qu'on parse soit dans le mémoire autorisée en 
  // écriture
  char* l = malloc(strlen(line)+1);
  l = strncpy(l, line, strlen(line)+1);

  parse = strtok(l, ",");
  if (parse == NULL) exit_msg(error_msg, 0);
  data->name = malloc(strlen(parse)+1);
  strcpy(data->name, parse);

  parse = strtok(NULL, ",");
  if (parse == NULL) exit_msg(error_msg, 0);
  data->activity = malloc(strlen(parse)+1);
  strcpy(data->activity, parse);

  parse = strtok(NULL, ",");
  if (parse == NULL) exit_msg(error_msg, 0);
  data->day = string_to_day(parse);

  parse = strtok(NULL, "\n");
  if (parse == NULL) exit_msg(error_msg, 0);
  data->hour = atoi(parse);
  free(l); 

  return data;
  }


int is_ok_size_string_data(Data* data){
    return strlen(data->activity) <= MAX_STRLEN_ACTIVITY && strlen(data->name) <= MAX_STRLEN_NAME && data->hour<=24;
}

//La fonction add_data  retourne 0 si l'opération s'est bien déroulé
//sinon -1
int add_data(Data* data) {
    int ret = -1;
    // Take semaphore before opening the file
    if(sem_wait(sem) != 0){
      perror("sem_wait in add_data");
      return ret;
    }
    FILE * dataFile = fopen("data", "a+"); //Appending to the file to keep the precedent entries
    if(dataFile==NULL) goto end;
    
    if(!is_ok_size_string_data(data)) goto end;

    //The format used is name,activity,day,hour
    fprintf(dataFile, "%s,%s,%s,%d\n", data->name, data->activity, day_to_string(data->day), data->hour);

    ret = 0;
    // Release all the ressources
end:
    if(dataFile) fclose(dataFile);
    sem_post(sem);
    return ret;
}

//Calculate the number of character an entry would take taking in accound the '\0'
size_t size_entry(Data* data) {
    return strlen(data->name) 
	+ strlen(data->activity)
	+ strlen(day_to_string(data->day))
	+ 2
	+ 4
	+ 1;
}

//Enlève la donnée data de la base de donnée
int delete_data(Data* data) {
    int ret = -1; //Value used in the goto
    if(sem_wait(sem) != 0){
      perror("sem_wait in delete_data");
      return ret;
    }
    size_t size = size_entry(data);
    int deleted_once = 0;

    if(rename("data", "oldData")) goto err;

    FILE* oldDataFile = fopen("oldData", "r");
    if(oldDataFile == NULL) goto err;

    FILE * dataFile = fopen("data", "w"); //Reading from the beginning of the file to find the entry
    if(dataFile==NULL) goto err;
     
    char * line = malloc(size * sizeof(char));
    if(line == NULL) goto err;
    
    char* lineCmp = malloc(size * sizeof(char));
    if(lineCmp == NULL) goto err;

    data_format(lineCmp, data);

    while(fgets(line, size, oldDataFile)){
	if(strcmp(line, lineCmp) != 0) {
	    if(fputs(line, dataFile) == 0) goto err;
	} else {
	    deleted_once = 1;
	}
    }

    if(!deleted_once) goto err;

    ret = 0;
    goto end;

err:
    fprintf(stderr, "Could not delete file entry %s,%s,%s,%d\n", data->name, data->activity, day_to_string(data->day), data->hour);
end:
    if(line) free(line);
    if(lineCmp) free(lineCmp);
    if(dataFile) fclose(dataFile);
    if(oldDataFile) fclose(oldDataFile);
    remove("oldData");
    sem_post(sem);
    return ret;
}

//Affiche le planning
int see_all(void) {
    int ret = -1; //Value used in goto
    if(sem_wait(sem) != 0){
      perror("sem_wait in see_all");
      return -1;
    }
    FILE * dataFile = fopen("data", "r");
    if(dataFile == NULL) goto err;
    
    size_t size = MAX_STRLEN_ENTRY + 1;
    char * line = malloc(size * sizeof(char));
    if(line == NULL) goto err;
    // We go through the file line per line and print each line
    while(fgets(line, size, dataFile)){
	Data * data = get_data(line);	
	printf("%s %dh: %s a %s\n", day_to_string(data->day), data->hour, data->name, data->activity);
	data_free(data);
    }

    ret = 0;
    goto end;

err:
    fprintf(stderr, "Error printing the data\n");
end:
    if(dataFile) fclose(dataFile);
    sem_post(sem);
    if(line) free(line);
    return ret;
}

enum Cmd {
    CMD_SEE, CMD_ADD, CMD_DEL, CMD_NONE
};
// Produce the CMD enum for each input string 
// And fill the data structure
enum Cmd parse_cmd(int argc, char** argv, struct Data * data){
    if(argc < 2)
	return CMD_NONE;

    if(strcmp("SEE", argv[1]) == 0)
	return CMD_SEE;
    
    // Not enough arguments
    if(argc < 6)
	return CMD_NONE;
    
    enum Day day = string_to_day(argv[4]);
    if(day == NONE)
	return CMD_NONE;
    
    int hour;
    if(sscanf(argv[5], "%d", &hour) != 1)
	return CMD_NONE;

    data->name = argv[2];
    data->activity = argv[3];
    data->day = day;
    data->hour = hour;

    if(strcmp("ADD", argv[1]) == 0)
	return CMD_ADD;

    if(strcmp("DEL", argv[1]) == 0)
	return CMD_DEL;

    return CMD_NONE;
}

int main(int argc, char** argv) {
    int ret = 0; // Used in the default case

    char * usage_line =  "Usage :\n\tbdd CMD [args...]\nWith CMD amongs\n\tSEE\n\tADD\n\tDEL\n";
    
    Data data;

    enum Cmd cmd = parse_cmd(argc, argv, &data);
    
    sem = sem_open(SEM_NAME, 0);
    if(sem == SEM_FAILED) {
      perror("sem_open in main");
      return 0;
    }

    switch(cmd){
	case CMD_SEE:
	    ret = see_all();
	    break;
	case CMD_ADD:
	    ret = add_data(&data);
	    break;
	case CMD_DEL:
	    ret = delete_data(&data);
	    break;
	default:
	    fprintf(stderr, "%s", usage_line);
	    break;
    }
    
    sem_close(sem);
    return ret;
}
