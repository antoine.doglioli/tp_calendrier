#include <sys/types.h>
#include <signal.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <semaphore.h>
#include "utils.h"
#include "unistd.h"
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>

char* bdd_bin = "./bdd";
sem_t * sem = NULL;
int cont = 1;
//On prépare les arguments qui seront envoyés à bdd
//ADD toto poney lundi 3 -> { "./bdd", "ADD", "toto", "poney", lundi", "3", NULL }
char** parse(char* line) {
  char** res = malloc(7 * sizeof(char*));
  res[0] = bdd_bin;

  char* arg1 = strtok(line, " ");
  res[1] = arg1;

  char* arg2 = strtok(NULL, " ");
  res[2] = arg2;
  if (arg2 == NULL) {
    arg1[strlen(arg1)] = '\0';
    return res;
  }

  char* arg3 = strtok(NULL, " ");
  res[3] = arg3;
  if (arg3 == NULL) { 
    arg2[strlen(arg2)-1] = '\0';
    return res;
  }

  char* arg4 = strtok(NULL, " ");
  res[4] = arg4;
  if (arg4 == NULL) {
    arg3[strlen(arg3)-1] = '\0';
    return res;
  } 
  
  char* arg5 = strtok(NULL, "\n");
  res[5] = arg5;
  res[6] = NULL;
  return res;
}

//Configuration de la socket réseau, retourne le file descriptor de la socket
int configure_socket() {
  int ret = -1;
  int socket_desc;

  socket_desc = socket(AF_INET, SOCK_STREAM, 0);
  
  if(socket_desc == -1){
    perror("Error in configure_socket()");
    goto out;
  }

  struct sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(SERVER_PORT);

  int res = bind(socket_desc, (struct sockaddr *) &address, sizeof(address));
  if(res != 0){
    fprintf(stderr, "Error configure_socket() binding : %d\n", res);
    perror("bind() in configure_socket() ");
    goto out;
  }

  ret = socket_desc;

out:
  return ret;
}

//Didn't saw that the parse function was already written ...
void build_argv(char * str, char ** argv){

  argv[0] = bdd_bin;
  
  size_t cursor = 0;
  size_t i = 1;

  // We just split the string at each space
  while(i<6 && str[cursor] != '\0'){
    argv[i] = str + cursor;
    while(str[cursor] != '\n' && str[cursor] != ' ' && str[cursor] != '\0'){
      cursor ++;
    }
    if(cursor == '\0') break;
    
    str[cursor] = '\0';
    cursor++;

    i++;
  }
  // The last arg need to be NULL 
  argv[i] = NULL;
}

// Passage des commandes à la base de données par un pipe
// Renvoi des réponses au client par la socket réseau
void  * process_communication(void * arg) {
  char cmd_str[LINE_SIZE] = {0};
  char reply_str[REPLY_SIZE] = {0};

  int socket_recept = (int) arg;

  // We get the command string from the client
  if(recv(socket_recept, cmd_str, LINE_SIZE, 0) == -1){
    // The connection is terminated we quit the threa
    goto end;
  }

  char *argv[7];
  build_argv(cmd_str, argv);  


  // We create the pipe 
  int fds[2];
  pipe(fds);

  // Then we fork and what follow is the classical...
  pid_t pid = fork();
  if(pid == 0){
    // We close one end of the pipe 
    close(fds[0]);
    // Then connect the other end to the stdout
    dup2(fds[1], STDOUT_FILENO);
    // Let's launch the bdd.....
    execv(bdd_bin, argv);
  }
  else if(pid >0){
    // We keep the other end of the pipe
    close(fds[1]);
    int status;
    
    // We wait for the child to end
    wait(&status);
    if(status != 0){
      fprintf(stderr, "cmd failed with error code %d\n", status);
    }
    else{
      // If successfull we read in the pipe the response of the child
      // Then we send the data read through the socket
      read(fds[0], reply_str, sizeof(reply_str));
      fprintf(stdout, "%s" , reply_str);

      if(send(socket_recept, reply_str, sizeof(reply_str), 0) < 0){
	perror("send() in process_communication():");
	return NULL;
      }
      fprintf(stdout, "cmd executed successfully\n"); 
    }
    close(fds[0]);
  }
  else{
      fprintf(stderr, "Failed to fork the programm\n");
  }
end:
  close(socket_recept);
  return NULL;
}

void intHandler() {
  sem_unlink(SEM_NAME);
  sem_close(sem);
  exit(0);
}

int main(int argc, char** argv) {
  int socket_desc;

  signal(SIGINT, intHandler);

  // Configuration de la socket serveur
  socket_desc = configure_socket();
  if(socket_desc == -1) goto out;
  // Réception des connections réseaux entrantes

  sem = sem_open(SEM_NAME, O_CREAT|O_EXCL, SEM_MODE, 1);

  if(sem == SEM_FAILED){
    perror("sem_open() in main():");
    goto out;
  }

  while(cont){
    printf("We listen\n");

    if(listen(socket_desc, 50) == -1){
      perror("Error main(), listen");
      goto out;
    }

    struct sockaddr_in addr_rececpt;
    socklen_t size_addr_recpt = sizeof(addr_rececpt);
    int socket_recept = accept(socket_desc, (struct sockaddr *) &addr_rececpt, &size_addr_recpt);

    if(socket_recept == -1){
      perror("Error main(), accept");
      goto out;
    }

    pthread_t thread;

    pthread_create(&thread, NULL, process_communication, (void *) socket_recept);

    pthread_detach(thread);
  }

  // Gestion des commandes entrantes dans la nouvelle socket

  /*end hide*/
out:
  sem_unlink(SEM_NAME);
  sem_close(sem);
  close(socket_desc);
  return 0;
}
