#include "utils.h"
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int main(int argc, char** argv) {
  
    // We prompt to enter command
  printf("cmd>");
  fflush(stdout);

  char cmd[LINE_SIZE];
  fgets(cmd, LINE_SIZE, stdin);

  char reply_str[REPLY_SIZE] = {0};

  while(strcmp(cmd, "quit") != 0){
    int sock = socket(AF_INET, SOCK_STREAM, 0); 
    if(sock < 0) {
      perror("socket() in main()");
      return -1;
    }
    
    struct sockaddr_in addr_send;
    addr_send.sin_family = AF_INET;
    addr_send.sin_port = htons(SERVER_PORT);
    addr_send.sin_addr.s_addr = INADDR_ANY;
    socklen_t size_addr_send = sizeof(addr_send);
    if(0 != connect(sock, (struct sockaddr *) &addr_send, size_addr_send)){
	perror("connect() in main()");
	return -1;
    }

    if(send(sock, cmd, sizeof(cmd), 0) < 0){
      perror("send() in main()");
      return -1;
    }

    printf("Sent a command to the server\n");

    if(recv(sock, reply_str, sizeof(reply_str), 0) < 0){
      perror("recv() in main():");
      return -1;
    }
   
    fputs(reply_str, stdout);
    
    printf("cmd>");
    fflush(stdout);
    fgets(cmd, LINE_SIZE, stdin);
    close(sock);
  }

  return 0;
}
